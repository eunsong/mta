sap.ui.define([], function () {
	"use strict";
	return {
		automodeCheck : function(mode){
			if(mode){
				return 'Information';
			}else{
				return 'None';
			}
		}
	};
});