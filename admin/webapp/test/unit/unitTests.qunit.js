/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"com/istn/ci/iVISION/config/admin/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});