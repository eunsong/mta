sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	'sap/ui/model/Filter',
	"../model/formatter"
], function (BaseController, JSONModel, MessageBox, Filter, formatter) {
	"use strict";

	return BaseController.extend("com.istn.ci.iVISION.config.admin.controller.Main", {
		
		formatter : formatter,
		
		globalConfig: {
			autoMode : [
				{text : "기본", key : "AutomateBasic" },
				{text : "이메일", key : "AutomateEmail" },
				{text : "RPA", key : "AutomateRPA" }]
		},
		
		onInit: function () {
			this._initModels();
			this.Router = this.getRouter();
			this.getOwnerComponent().reload = true;
			this.getRouter().getRoute("Main").attachMatched(this._onRouteMatchedMain, this);
		},

		_onRouteMatchedMain: function(event) {
			if(this.getOwnerComponent().reload){
				this._getCustomerData();
				this.getOwnerComponent().reload = false;
			}
		},
		
		_initModels: function(){
			this.getView().setModel(new JSONModel(), "filterData");
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			this.getView().setModel(new JSONModel(this.globalConfig.autoMode), "automateList");	
		},
		
		_getCustomerData: function(){
			var that = this;
			$.ajax({
				url: "/iVISION/Admin/Customers?$expand=_systems($expand=_users)",
				success: function(data, headers){
					data.value.forEach(customer=>{
						var cnt = 0;
						var registerNum = customer.RegistrationNumber;
						customer.RegistrationNumber = registerNum.substring(0,3) + "-" + registerNum.substring(3,5) + "-" + registerNum.substring(5)
						customer._systemCount = customer._systems.length;
						customer._systems.forEach(function(system){
							cnt += system._users.length;
							
							//system Type Text
							switch(system.Type){
								case 1:
									system._system_type_text = {key: system.Type, text: "S4HANA"};
									break;
								case 2:
									system._system_type_text = {key: system.Type, text: "S4HANACLOUD"};
									break;
								case 3:
									system._system_type_text = {key: system.Type, text: "RPA_TRIGGER"};
									break;
								case 4:
									system._system_type_text = {key: system.Type, text: "RPA_NOTIFIRE"};
									break;
							}
							
							//user > system type text setting
							system._users.forEach(function(user){
								switch(user._systems_Type){
									case 1:
										user.typeText = "S4HANA";
										break;
									case 2:
										user.typeText = "S4HANACLOUD";
										break;
									case 3:
										user.typeText = "RPA_TRIGGER";
										break;
									case 4:
										user.typeText = "RPA_NOTIFIRE";
										break;
								}
							})
						})
						customer._userCount = cnt;
					})
					that.getOwnerComponent().customerData = data.value;
					that.getView().setModel(new JSONModel(data.value), "filterTable");
				},
				error: function(error){
					MessageBox.error(that.i18n.getText("main.error"));
				}
			});
		},
		
		onChange: function(event){
			this.getView().byId("filterTable").setBlocked(true);
			
			//사업자 등록번호는 10자리 입력 후 검색 가능
			if(event.getParameter("id").indexOf("RegistrationNumber") !== -1 ){
				var registerNumberLen = event.getParameter("value").replace(/_/g, "").length;
				if(registerNumberLen !== 12 && registerNumberLen > 0 ){
					return MessageBox.warning(this.i18n.getText("main.registerationNumberLength"));
				}
			}
		},
		
		onSearch: function(event){
			var filters = [];
			var filterTable = this.getView().byId("filterTable");

			event.getParameter("selectionSet").forEach(item=>{
				var id = item.getId().split("--").reverse()[0];
				if(id === "CustomerName" && item.getValue()){
                    filters.push(new Filter(id, sap.ui.model.FilterOperator.Contains, item.getValue()))
				}
				if(id === "RegistrationNumber" && item.getValue()){
                    filters.push(new Filter(id, sap.ui.model.FilterOperator.EQ, item.getValue()))
				}
				if(id === "automateMode" && item.getSelectedKeys().length > 0){
					var automodeFilters = item.getSelectedKeys().map(automode=>{
						return new Filter(automode, sap.ui.model.FilterOperator.EQ, true);
					})
					filters = filters.concat(automodeFilters);
				}
			})
			
            filterTable.getBinding("items").filter(filters);
			filterTable.setBlocked(false);
		},

		onCreate: function(){
			this.getRouter().navTo("Create");
		},
		
		onList: function(event){
			var listIdx = event.getSource().getBindingContextPath().split("/").reverse()[0];
			var detailData = this.getView().getModel("filterTable").getData()[listIdx];
			this.getOwnerComponent().customerDetail = JSON.parse(JSON.stringify(detailData));
			this.getRouter().navTo("Detail");
		},
		
		onErrorLog: function(){
			MessageBox.show(this.i18n.getText("main.errorLog"));
		},

	});

});