sap.ui.define([
	"./BaseController",
	"sap/ui/core/Fragment",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"../model/formatter"
], function (BaseController, Fragment, JSONModel, MessageBox, formatter) {
	"use strict";

	return BaseController.extend("com.istn.ci.iVISION.config.admin.controller.Detail", {
		
		formatter: formatter,
		
		globalConfig: {
			systemType : [
				{text : "S4HANA", key : 1 },
				{text : "S4HANACLOUD", key : 2 },
				{text : "RPA_TRIGGER", key : 3 },
				{text : "RPA_NOTIFIRE", key : 4 }]
		},
		
		onInit: function () {
			this.Router = this.getRouter();
			this.getRouter().getRoute("Detail").attachMatched(this._onRouteMatchedDetail, this);
			this._initTemplates();
			this._initialization();
		},
		
		_initTemplates: function(){
			this.ReadonlyTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.iVISION.config.admin.view.Readonly", this).reverse();
			this.EditableTemplate = sap.ui.xmlfragment(this.getView().getId(), "com.istn.ci.iVISION.config.admin.view.Editable", this).reverse();
		},

		
		_initialization: function(){
			this.getView().setModel(new JSONModel(this.globalConfig), "globalConfig");
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
			this._showFormFragment("Readonly");
		},
		
		_onRouteMatchedDetail: function(){
			if(!this.getOwnerComponent().customerDetail){
				return this.getRouter().navTo("Main");
			}
			this._initFields();
			var customer = this.getOwnerComponent().customerDetail;
			delete customer._systemCount;
			delete customer._userCount;
			customer._users = [];
			customer._systems.forEach(function(system){
				system.editable = false;
				system._users.forEach(function(user){
					user.editable = false;
					customer._users.push(user);
				})
				delete system._users
			});
			if(!this.getView().getModel("customer")){
				this.getView().setModel(new JSONModel(customer), "customer");
				this.getView().setModel(new JSONModel($.extend(true, {}, customer)), "originCustomer");
			}else{
				this.getView().getModel("customer").setData(customer);
				this.getView().getModel("originCustomer").setData($.extend(true, {}, customer));
			}
		},
		
		_initFields: function(event){
			var elements = document.getElementsByClassName("mandatoryFields") ;
			Object.keys(elements).forEach(function(key) {
				var tmpElement = elements[key];
				var realElement = sap.ui.getCore().byId(tmpElement.getAttribute('id'));
				realElement.setValueState(sap.ui.core.ValueState.None);
			});
		},
		
		onNavBack: function(event){
			var that = this;
			var editableBtn = this.getView().byId("editableBtn");
			var customer = this.getView().getModel("customer").getData();
			if(this._isDataChanged(customer)){
				MessageBox.alert(this.i18n.getText("detail.cancelWithData"),{
					actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (oAction) { 
						if(oAction === "OK"){
							that.Router.navTo("Main");
							that._showFormFragment('Readonly');
						}
					}
				});
			}else{
				this.Router.navTo("Main");
				if(!editableBtn.getVisible()){ this._showFormFragment('Readonly'); }
			}
		},
		
		_showFormFragment: function(mode){
			var page = this.byId("detailPage"), editBtn = this.byId("editableBtn"), deleteBtn = this.byId("deleteBtn");
			if(page.getSections().length > 0) page.removeAllSections();

			var template = this[`${mode}Template`];
			if(!template) return MessageBox.error(this.i18n.getText("detail.MSG_FAIL_LOAD_TEMPLATE")); 
			template.forEach(function(section){ page.insertSection(section) });

			this.getView().byId(`${mode}_customerName`).focus()	//페이지 이동 시, focus 적용
			page.setShowFooter(!page.getShowFooter());
			editBtn.setVisible(!editBtn.getVisible());
			deleteBtn.setVisible(!deleteBtn.getVisible());
		},
		
		/*
		임시 Client 생성 Function
		*/
		onClientKey: function(event){
			var customerModel = this.getView().getModel("customer");
			var customer = customerModel.getData();
			customer.ClientID = "I-Client-Id-TESTKEY";                         
			customer.ClientSecret = "I-Client-Secret-TESTKEY";
			customerModel.refresh();
		},
		
		onAddItem: function(event){
			var id = event.getSource().getId().split("-").reverse()[0];
			var customerModel = this.getView().getModel("customer"); 
			var customerData = customerModel.getData(); 
			customerData[`${id}`].push({});
			customerModel.refresh();
		},
		
		onDeleteSystem: function(event){
			var that = this;
			var cnt = 0;
			var deleteBtn = event.getSource();
			var customerModel = this.getView().getModel("customer");
			var table = deleteBtn.getParent().getParent(), selectedItems = table.getSelectedItems();
			
			table.getSelectedContextPaths().forEach(function(item){
				var idx = item.split("/").reverse()[0];
				if(customerModel.getData()["_systems"][idx]['ID']){
					cnt++;
				}
			})
			if(cnt>0){
				MessageBox.warning(this.i18n.getText("detail.deleteSystem"),{
					actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (oAction) { 
						if(oAction === "OK"){
							that._deleteSystemUser(deleteBtn, customerModel, selectedItems);
						}
					}
				});
			}else{
				this._deleteSystemUser(deleteBtn, customerModel, selectedItems);
			}
			
		}, 
		
		_deleteSystemUser: function(deleteBtn, customerModel, selectedSystems){
			var customer = customerModel.getData();
			var deleteUserIdx = [];
			
			selectedSystems.forEach(function(item){
				var systemIdx = item.getBindingContextPath().split("/").reverse()[0];
				customer._users.forEach(function(user, idx){
					if(user._systems_Endpoint === customer._systems[systemIdx].Endpoint && user._systems_Type === customer._systems[systemIdx].Type){
                        deleteUserIdx.push(idx);
					}
				})
			})
			
			//해당 system의 user 삭제
			for (var i = deleteUserIdx.length - 1; i >= 0; --i) {
				customer['_users'].splice(deleteUserIdx[i], 1);
			}
			
			//system 삭제
			for (var i = selectedSystems.length - 1; i >= 0; --i) {
				var idx = selectedSystems[i].getBindingContextPath().split("/").reverse()[0];
				customer['_systems'].splice(idx, 1);
				selectedSystems[i].setSelected(false);
			}
			
			customerModel.refresh();
			deleteBtn.setEnabled(false);
		},
		
		onDeleteUser: function(event){
			var deleteBtn = event.getSource();
			var table = deleteBtn.getParent().getParent(), selectedItems = table.getSelectedItems();
			var customerModel = this.getView().getModel("customer");
			var customer = customerModel.getData();
			var dbUser = 0;
			
			table.getSelectedContextPaths().forEach(function(item){
				var idx = item.split("/").reverse()[0];
				if(customer["_users"][idx]['_systems_ID']){
					dbUser++;
				}
			})
			
			if(dbUser > 0){
				MessageBox.warning(this.i18n.getText("detail.deleteUser"),{
					actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (oAction) { 
						if(oAction === "OK"){
							for (var i = selectedItems.length - 1; i >= 0; --i) {
								var idx = selectedItems[i].getBindingContextPath().split("/").reverse()[0];
								customer['_users'].splice(selectedItems[i], 1);
								selectedItems[i].setSelected(false);
							}
							customerModel.refresh();
							deleteBtn.setEnabled(false);
						}
					}
				})
			}else{
				for (var i = selectedItems.length - 1; i >= 0; --i) {
					var idx = selectedItems[i].getBindingContextPath().split("/").reverse()[0];
					customer['_users'].splice(selectedItems[i], 1);
					selectedItems[i].setSelected(false);
				}
				customerModel.refresh();
				deleteBtn.setEnabled(false);
			}
		},
		
		onSelectChange: function(event){
			var table = event.getSource(), selectedItems = table.getSelectedItems();
			var removeBtn = table.getHeaderToolbar().getContent().find(function(item){ return item.getId().indexOf('delete') != -1});	  
			removeBtn.setEnabled(selectedItems.length > 0);
		},
		
		onEdit: function(event){
			this._showFormFragment('Editable');
			this.getView().byId("saveBtn").setEnabled(true);
		},
		
		onDelete: function(event){
			var that = this;
			var customer = this.getView().getModel("customer").getData();
			MessageBox.warning(this.i18n.getText("detail.deleteCustomer"), {
				actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
				emphasizedAction: MessageBox.Action.OK,
				onClose: function (oAction) { 
					if(oAction === "OK"){
						that.getView().byId("detailPage").setBusy(true);
						that._deleteCustomer(customer);
					}
				}
			});
		},
		
		onCancel: function(event){
			var that = this;
			var customer = this.getView().getModel("customer").getData();
			var originCustomer = this.getView().getModel("originCustomer").getData();
			if(this._isDataChanged(customer)){
				MessageBox.alert(this.i18n.getText("detail.cancelWithData"),{
					actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (oAction) { 
						if(oAction === "OK"){
							that.getView().getModel("customer").setData($.extend(true, {}, originCustomer));
							that._showFormFragment('Readonly');
						}
					}
				});
			}else{
				this._showFormFragment('Readonly');
			}
		},
		
		onChange: function(event){
			var source = event.getSource();
			var id = event.getParameter("id");
			var value = event.getParameter("value");
			var reg = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
			
			if(id.indexOf("email") != -1 && !reg.test(value)){
				source.setValueState("Error");
				source.setValueStateText(this.i18n.getText("detail.emailValid"));
				return;
			}

			if(value){
				source.setValueState("None");
			}else{
				source.setValueState("Error");
				source.setValueStateText(this.i18n.getText("detail.mandatoryField"));
			}
		},
		
		onCustomerNameChange: function(event){
			var nameReg = /^[가-힣a-zA-Z]+$/;
			if(event.getParameter("value") && !nameReg.test(event.getParameter("value"))){
				event.getSource().setValueState("Error");
				MessageBox.warning(this.i18n.getText("detail.customerNameValid"));
			}else{
				event.getSource().setValueState("None");
			}
		},
		
		onTargetChange: function(event){
			var targetReg = /^[a-zA-Z]+$/;
			if(event.getParameter("value") && !targetReg.test(event.getParameter("value"))){
				event.getSource().setValueState("Error");
				MessageBox.warning(this.i18n.getText("detail.systemTargetValid"));
			}else{
				event.getSource().setValueState("None");
			}
		},
		
		onDuplicateSystem: function(event){
			var systems = this.getView().getModel("customer").getData()["_systems"];
			if(event.getParameter("id").indexOf("endpoint") != -1){
				var domainReg = /(http(s)?:\/\/)([a-z0-9\w]+\.)+[a-z0-9]{2,4}$/gi;
				if(event.getParameter("value") && !domainReg.test(event.getParameter("value"))){
					event.getSource().setValueState("Error");
					return MessageBox.warning(this.i18n.getText("detail.endpointValid"));
				}else{
					event.getSource().setValueState("None");
					
				}
			}else{
				systems.forEach(function(system){
					system._system_type_text = this._getSystemTypeText(system);
				})
			}
			
			
			var tempArr = [];
			systems.forEach(function(system){
				if(typeof(system.Endpoint) === "string" && typeof(system.Type) === "number"){
					tempArr.push(system.Endpoint + system.Type);
				}
			})
			
			var duplicateArr = new Set(tempArr);
			if(tempArr.length != duplicateArr.size){
				event.getSource().setValueState("Error");
				event.getSource().setSelectedItem(null);
				return MessageBox.warning(this.i18n.getText("detail.duplicateSystem"));
			}else{
				event.getSource().setValueState("None");
			}
		}, 
		
		onDuplicateUser: function(event){
			var source = event.getSource();
			var id = event.getParameter("id");
			var value = event.getParameter("value");
			var emailReg = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
			
			if(id.indexOf("email") != -1 && value && !emailReg.test(value)){
				source.setValueState("Error");
				return MessageBox.warning(this.i18n.getText("detail.emailValid"));
			}
			
			var users = this.getView().getModel("customer").getData()["_users"];
			var tempArr = [];
			users.forEach(function(user){
				if(typeof(user._systems_Endpoint) === "string" && typeof(user._systems_Type) === "number" && typeof(user.Email) === "string"){
    				tempArr.push(user._systems_Endpoint + user._systems_Type + user.Email);				
				}
			})
			var duplicateArr = new Set(tempArr);
			if(tempArr.length != duplicateArr.size){
				event.getSource().setValueState("Error");
				event.getSource().setSelectedItem(null);
				return MessageBox.warning(this.i18n.getText("detail.duplicateUser"));
			}else{
				event.getSource().setValueState("None");
			}
		}, 
		
		onSave: function(event){
			var that = this;
			var customer = this.getView().getModel("customer").getData();
			
			if(!this._isDataChanged(customer)){
				MessageBox.alert(this.i18n.getText("detail.noChangedData"));
			}else{
				if(this._checkMandatory()){
					MessageBox.confirm(this.i18n.getText("detail.saveCustomer"), {
						actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
						emphasizedAction: MessageBox.Action.OK,
						onClose: function (oAction) { 
							if(oAction === "OK"){
								that.getView().byId("detailPage").setBusy(true);
								that._reorganizeData(JSON.parse(JSON.stringify(customer)));
							}
						}
					});
				}
			}
			
		},
		
		_isDataChanged: function(data){
			var that = this;
			var originCustomer = this.getView().getModel("originCustomer").getData();
			var originSystems = originCustomer['_systems'];
			var originUsers = originCustomer['_users'];
			
			for(var key in data){
				if(key === "_systems"){
					//시스템 데이터 바뀐 경우
					if(originSystems.length === data[`${key}`].length){
						var	diffSystem = originSystems.find(function(dbSystem, idx){
							if(data[`${key}`][idx].Target1 === "") data[`${key}`][idx].Target1 = null;
							if(data[`${key}`][idx].Target2 === "") data[`${key}`][idx].Target2 = null;
							if(data[`${key}`][idx].Target3 === "") data[`${key}`][idx].Target3 = null;
							return ( dbSystem.Target1 !== data[`${key}`][idx].Target1 || 
								dbSystem.Target2 !== data[`${key}`][idx].Target2 || dbSystem.Target3 !== data[`${key}`][idx].Target3 )
						})
						if(diffSystem) return true;
					}else{
						return true;
					}
				}else if(key === "_users"){
					//사용자 데이터 바뀐 경우
					if(originUsers.length === data[`${key}`].length){
						var	diffUser = originUsers.find(function(dbUser, idx){
							if(data[`${key}`][idx].FullName === "") data[`${key}`][idx].FullName = null;
							return dbUser.FullName !== data[`${key}`][idx].FullName;
						})
						if(diffUser) return true;
					}else{
						return true;
					}
				}else{
					//고객 정보가 변경된 경우
					if(data[`${key}`] === "") data[`${key}`] = null;
					if(originCustomer[`${key}`] !== data[`${key}`]) return true;
				}
			}
			
			//어느 데이터도 바꾸지 않은 경우
			return false;
		},
		
		//필수 입력값, 유효성 체크
		_checkMandatory: function(event){
			var chkMandResult = true;
			var chkMandCnt = 0;
			var chkInvalidInputCnt = 0;
			var elements = document.getElementsByClassName("mandatoryFields");
			Object.keys(elements).forEach(function(key) {
				var tmpElement = elements[key];
				var realElement = sap.ui.getCore().byId(tmpElement.getAttribute('id'));
				var type = realElement.getMetadata().getElementName().split(".").reverse()[0]
				switch(type){
					case "Input" :
						if(realElement.getValue() === ""){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							//유효하지 않는 기존 입력값이 있는 경우 저장X
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
						
					case "MaskInput":
						if(realElement.getValue() === ""){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
						
					case "Select":
						if(realElement.getSelectedKey() === ""){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
				}
				
			});
		 
			if(chkInvalidInputCnt > 0){
				chkMandResult = false;
				MessageBox.error(this.i18n.getText("detail.checkValidInput"));
			}else if(chkMandCnt > 0){
				chkMandResult = false;
				MessageBox.error(this.i18n.getText("detail.checkAllMandatoryFields"));
			 }
			 return chkMandResult;
		},
		
		//데이터 변경
		_reorganizeData: function(customer){
			var cnt = 0;
			customer.RegistrationNumber = customer.RegistrationNumber.replace(/[^0-9]/g, "");
			 customer._systems.forEach(function(system){
			 	delete system.editable;
			 	delete system._system_type_text;
			 	customer._users.forEach(function(user){
			 		if(user._systems_Endpoint === system.Endpoint && user._systems_Type === system.Type){
			 			cnt++;
			 			delete user.editable;
			 			delete user.typeText;
			        	if(!system._users){
			        		system._users = [];
			        		delete user._systems_Endpoint
			        		delete user._systems_Type
							system._users.push(user);
			        	}else{
			        		delete user._systems_Endpoint
			        		delete user._systems_Type
							system._users.push(user);
			        	}
			        }
			 	})
			 })

		    if(cnt === customer._users.length){
				delete customer._users
				this._updateCustomer(customer);
		    }else{
				MessageBox.warning(this.i18n.getText("detail.notExistSystem"));
				this.getView().byId("detailPage").setBusy(false);
		    }
		    
		},
		
		/*
			system type text setting
		*/
		_getSystemTypeText: function(system){
			switch(system.Type){
				case 1:
					system._system_type_text = {key: system.Type, text: "S4HANA"};
					break;
				case 2:
					system._system_type_text = {key: system.Type, text: "S4HANACLOUD"};
					break;
				case 3:
					system._system_type_text = {key: system.Type, text: "RPA_TRIGGER"};
					break;
				case 4:
					system._system_type_text = {key: system.Type, text: "RPA_NOTIFIRE"};
					break;
			}
			return system._system_type_text;
		},
		
		
		_deleteCustomer: function(customer){
			var that = this;
			var consdition = `(ID='${customer.ID}',RegistrationNumber='${customer.RegistrationNumber.replace(/[^0-9]/g, "")}')`;
			$.ajax({
				url: "/CI_IVISION_ODATA_V4/Admin/Customers" + consdition,
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				method: "DELETE",
				success: function(data, headers){
					MessageBox.success(that.i18n.getText("detail.successDelete"), {
						actions: [MessageBox.Action.OK],
						emphasizedAction: MessageBox.Action.OK,
						onClose: function (oAction) { 
							if(oAction === "OK"){
								that._showFormFragment('Readonly');
								that.getOwnerComponent().reload = true;
								that.Router.navTo("Main");
							}
						}
					});
					that.getView().byId("detailPage").setBusy(false); 
				},
				error: function(error){
					MessageBox.error(that.i18n.getText("detail.error"));
					that.getView().byId("detailPage").setBusy(false); 
				}
			});	
		},
		
		_updateCustomer: function(customer){
			var that = this;
			var customerData = this.getView().getModel("customer").getData();
			var originCustomer = this.getView().getModel("originCustomer").getData();
			var consdition = `(ID='${customer.ID}',RegistrationNumber='${customer.RegistrationNumber}')`;
			$.ajax({
				url: "/CI_IVISION_ODATA_V4/Admin/Customers" + consdition,
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				data: JSON.stringify(customer),
				method: "PUT",
				success: function(data, headers){
					MessageBox.success(that.i18n.getText("detail.successUpdate"), {
						actions: [MessageBox.Action.OK],
						emphasizedAction: MessageBox.Action.OK,
						onClose: function (oAction) { 
							if(oAction === "OK"){
								customerData._systems.forEach(function(system){
									system.editable = false;
									system._system_type_text = that._getSystemTypeText(system);
								})
								customerData._users.forEach(function(user){
									user.editable = false;
									switch(user._systems_Type){
										case 1:
											user.typeText = "S4HANA";
											break;
										case 2:
											user.typeText = "S4HANACLOUD";
											break;
										case 3:
											user.typeText = "RPA_TRIGGER";
											break;
										case 4:
											user.typeText = "RPA_NOTIFIRE";
											break;
									}
								})
								that.getView().getModel("originCustomer").setData(JSON.parse(JSON.stringify(customerData)));
								that.getOwnerComponent().reload = true;
								that._showFormFragment('Readonly');
							}
						}
					});
					that.getView().byId("detailPage").setBusy(false); 
				},
				error: function(error){
					MessageBox.error(that.i18n.getText("detail.error"));
					that.getView().byId("detailPage").setBusy(false); 
				}
			});	
		},

		
		
	});

});