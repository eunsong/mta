sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function (BaseController, JSONModel, MessageBox) {
	"use strict";

	return BaseController.extend("com.istn.ci.iVISION.config.admin.controller.Create", {
		
		globalConfig: {
			systemType : [
				{text : "S4HANA", key : 1 },
				{text : "S4HANACLOUD", key : 2 },
				{text : "RPA_TRIGGER", key : 3 },
				{text : "RPA_NOTIFIRE", key : 4 }]
		},
		
		onInit: function () {
			this._initializtion();
			this.Router = this.getRouter();
			this.Router.getRoute("Create").attachMatched(this._onRouteMatchedCreate, this);
		},
		
		_onRouteMatchedCreate: function(event){
			if(!this.getOwnerComponent().customerData){
				return this.Router.navTo("Main");
			}
			var customerData = { AutomateBasic : true, _systems : [{}], _users : [{}] };
			this.getView().getModel("customer").setData(customerData);
			this.getView().byId("customerName").focus();
			this.getView().byId("system_type").setSelectedKey(1);
			this._initFields();
			this._createClientKey();
		},
		
		_initializtion: function(event){
			this.getView().setModel(new JSONModel(), "customer");
			this.getView().setModel(new JSONModel(this.globalConfig), "globalConfig");
			this.i18n = this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		
		_initFields: function(event){
			var elements = document.getElementsByClassName("mandatoryFieldsCreate") ;
			Object.keys(elements).forEach(function(key) {
				var tmpElement = elements[key];
				var realElement = sap.ui.getCore().byId(tmpElement.getAttribute('id'));
				realElement.setValueState(sap.ui.core.ValueState.None);
			});
		},
		
		onNavBack: function(event){
			var that = this;
			var customer = this.getView().getModel("customer").getData();
			
			if(this._isDataChanged(customer)){
				MessageBox.alert(this.i18n.getText("create.cancelWithData"),{
					actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (oAction) { 
						if(oAction === "OK"){
							that.Router.navTo("Main");
						}
					}
				});
			}else{
				this.Router.navTo("Main");
			}
		},
		
		_createClientKey: function(event){
			var customerModel = this.getView().getModel("customer");
			var customer = customerModel.getData();
			customer.ClientID = "I-Client-Id-TESTKEY";                         
			customer.ClientSecret = "I-Client-Secret-TESTKEY";
			customerModel.refresh();
			this.getView().setModel(new JSONModel($.extend(true, {}, customer)), "originCustomer")
		},
		
		onCustomerNameChange: function(event){
			var nameReg = /^[가-힣a-zA-Z0-9 ]+$/;
			if(event.getParameter("value") && !nameReg.test(event.getParameter("value"))){
				event.getSource().setValueState("Error");
				MessageBox.warning(this.i18n.getText("create.customerNameValid"));
			}else{
				event.getSource().setValueState("None");
			}
		},
		
		onRegisterNumberChange: function(event){
			var source = event.getSource();
			var id = event.getParameter("id");
			var value = event.getParameter("value");
			var value = event.getParameter("value");
			if(value.indexOf("_") != -1){
				source.setValueState("Error");
				MessageBox.warning(this.i18n.getText("create.registerationNumberLength"));
				return;
			}else{
				if(this._isExistRegistrationNumber(value)){
					source.setValueState("None");
				}else{
					source.setValueState("Error");
					MessageBox.warning(this.i18n.getText("create.existRegisterationNumber"));
					return;
				}
			}
		},
		
		//사업자 등록번호 중복 체크
		_isExistRegistrationNumber: function(value){
			var customers = this.getOwnerComponent().customerData;
			var chkExistNum = customers.find(function(customer){
				return value === customer.RegistrationNumber;
			})
			return !chkExistNum ? true : false;
		},
		
		onTargetChange: function(event){
			var targetReg = /^[a-zA-Z]+$/;
			if(event.getParameter("value") && !targetReg.test(event.getParameter("value"))){
				event.getSource().setValueState("Error");
				MessageBox.warning(this.i18n.getText("create.systemTargetValid"));
			}else{
				event.getSource().setValueState("None");
			}
		},
		
		onDuplicateSystem: function(event){
			var systems = this.getView().getModel("customer").getData()["_systems"];
			if(event.getParameter("id").indexOf("endpoint") != -1){
				var domainReg = /(http(s)?:\/\/)([a-z0-9\w]+\.)+[a-z0-9]{2,4}$/gi;
				if(event.getParameter("value") && !domainReg.test(event.getParameter("value"))){
					event.getSource().setValueState("Error");
					return MessageBox.warning(this.i18n.getText("create.endpointValid"));
				}else{
					event.getSource().setValueState("None");
				}
			}else{
				systems.forEach(function(system){
					switch(system.Type){
						case 1:
							system._system_type_text = {key: system.Type, text: "S4HANA"};
							break;
						case 2:
							system._system_type_text = {key: system.Type, text: "S4HANACLOUD"};
							break;
						case 3:
							system._system_type_text = {key: system.Type, text: "RPA_TRIGGER"};
							break;
						case 4:
							system._system_type_text = {key: system.Type, text: "RPA_NOTIFIRE"};
							break;
					}
				})
			}              
			var tempArr = [];
			systems.forEach(function(system){
				if(typeof(system.Endpoint) === "string" && typeof(system.Type) === "number"){
					tempArr.push(system.Endpoint + system.Type);
				}
			})
			var duplicateArr = new Set(tempArr);
			if(tempArr.length != duplicateArr.size){
				event.getSource().setValueState("Error");
				event.getSource().setSelectedItem(null);
				return MessageBox.warning(this.i18n.getText("create.duplicateSystem"));
			}else{
				event.getSource().setValueState("None");
			}
		}, 
		
		onDuplicateUser: function(event){
			var source = event.getSource();
			var id = event.getParameter("id");
			var value = event.getParameter("value");
			var emailReg = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
			
			if(id.indexOf("email") != -1 && value && !emailReg.test(value)){
				source.setValueState("Error");
				return MessageBox.warning(this.i18n.getText("create.emailValid"));
			}
			
			var users = this.getView().getModel("customer").getData()["_users"];
			var tempArr = [];
			users.forEach(function(user){
				if(typeof(user._systems_Endpoint) === "string" && typeof(user._systems_Type) === "number" && typeof(user.Email) === "string"){
    				tempArr.push(user._systems_Endpoint + user._systems_Type + user.Email);				
				}
			})
			var duplicateArr = new Set(tempArr);
			if(tempArr.length != duplicateArr.size){
				event.getSource().setValueState("Error");
				event.getSource().setSelectedItem(null);
				return MessageBox.warning(this.i18n.getText("create.duplicateUser"));
			}else{
				event.getSource().setValueState("None");
			}
		}, 
		
		onAddItem: function(event){
			var id = event.getSource().getId().split("-").reverse()[0];
			var customerModel = this.getView().getModel("customer"); 
			var customerData = customerModel.getData(); 
			customerData[`${id}`].push({});
			customerModel.refresh();
		},
		
		onDeleteItem: function(event){
			var deleteBtn = event.getSource();
			var id = deleteBtn.getId().split("-").reverse()[0];
			var table = deleteBtn.getParent().getParent(), selectedItems = table.getSelectedItems();
			var customerModel = this.getView().getModel("customer");
			var customer = customerModel.getData();
			var cnt = 0;
			
			selectedItems.forEach(function(item){
				var idx = item.getBindingContextPath().split("/").reverse()[0];
				customer[`${id}`].splice(idx-cnt, 1);
				item.setSelected(false);
				cnt++;
			})
			customerModel.refresh();
			deleteBtn.setEnabled(false);
			
		},
		
		onSelectChange: function(event){
			var table = event.getSource(), selectedItems = table.getSelectedItems();
			var removeBtn = table.getHeaderToolbar().getContent().find(function(item){ return item.getId().indexOf('delete') != -1});	  
			removeBtn.setEnabled(selectedItems.length > 0);
		},
		
		onCancel: function(evnet){
			this.onNavBack();
		},
		
		onSave: function(event){
			var that = this;
			var customer = this.getView().getModel("customer").getData();
			
			if(this._checkMandatory()){
			    MessageBox.confirm(this.i18n.getText("create.saveCustomer"), {
					actions: [MessageBox.Action.CANCEL, MessageBox.Action.OK],
					emphasizedAction: MessageBox.Action.OK,
					onClose: function (oAction) { 
						if(oAction === "OK"){
							that.getView().byId("createPage").setBusy(true);
							that._reorganizeData(JSON.parse(JSON.stringify(customer)));
						}
					}
				});
			}
		},
		
		_isDataChanged: function(data){
			var that = this;
			var originCustomer = this.getView().getModel("originCustomer").getData(); 

			for(var key in data){
				//시스템이 변경된 경우
				if(key === "_systems"){
					if(originCustomer[`${key}`].length === data[`${key}`].length){
						var	diffSystem = originCustomer[`${key}`].find(function(initSystem, idx){
							if(data[`${key}`][idx].Target1 === "") data[`${key}`][idx].Target1 = null;
							if(data[`${key}`][idx].Target2 === "") data[`${key}`][idx].Target2 = null;
							if(data[`${key}`][idx].Target3 === "") data[`${key}`][idx].Target3 = null;
							return ( initSystem.Endpoint !== data[`${key}`][idx].Endpoint || initSystem.Type !== data[`${key}`][idx].Type || 
								initSystem.Target1 !== data[`${key}`][idx].Target1 || initSystem.Target2 !== data[`${key}`][idx].Target2 || 
								initSystem.Target3 !== data[`${key}`][idx].Target3 )
						})
						if(diffSystem) return true;
					}else{
						return true;
					}
				//사용자가 변경된경우
				}else if(key === "_users"){
					if(originCustomer[`${key}`].length === data[`${key}`].length){
						var	diffUser = originCustomer[`${key}`].find(function(initUser, idx){
							if(data[`${key}`][idx].FullName === "") data[`${key}`][idx].FullName = null;
							return ( initUser._systems_Endpoint !== data[`${key}`][idx]._systems_Endpoint || 
								initUser._systems_Type !== data[`${key}`][idx]._systems_Type || 
								initUser.Email !== data[`${key}`][idx].Email || initUser.FullName !== data[`${key}`][idx].FullName);
						})
						if(diffUser) return true;
					}else{
						return true;
					}
				}else{
					//고객 정보가 변경된 경우
					if(data[`${key}`] === "") data[`${key}`] = null;
					if(originCustomer[`${key}`] !== data[`${key}`]) return true;
				}
			}
			
			//어느 데이터도 바꾸지 않은 경우
			return false;
		},
		
		_checkMandatory: function(event){
			//필수 입력값 체크
			var chkMandResult = true;
			var chkMandCnt = 0;
			var chkInvalidInputCnt = 0;
			var elements = document.getElementsByClassName("mandatoryFieldsCreate") ;

			Object.keys(elements).forEach(function(key) {
				var tmpElement = elements[key];
				var realElement = sap.ui.getCore().byId(tmpElement.getAttribute('id'));
				var type = realElement.getMetadata().getElementName().split(".").reverse()[0];
				
				switch(type){
					case "Input" :
						if(realElement.getValue() === ""){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							//유효하지 않는 기존 입력값이 있는 경우 저장X
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
						
					case "MaskInput":
						if(realElement.getValue() === ""){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
						
					case "Select":
						if(realElement.getSelectedKey() === ""){
							chkMandCnt++;
							realElement.setValueState(sap.ui.core.ValueState.Error);
						}else{
							realElement.getValueState() === "Error" ? chkInvalidInputCnt++ : realElement.setValueState(sap.ui.core.ValueState.None);
						}
						break;
				}
			});
		 
			if(chkInvalidInputCnt > 0){
				chkMandResult = false;
				MessageBox.error(this.i18n.getText("create.checkValidInput"));
			}else if(chkMandCnt > 0){
				chkMandResult = false;
				MessageBox.error(this.i18n.getText("create.checkAllMandatoryFields"));
			 }
			 return chkMandResult;
		},
		
		_reorganizeData: function(customer){
			var cnt = 0;

			customer._users.forEach(function(user){
				customer._systems.forEach(function(system){
					delete system._system_type_text;
			        if(user._systems_Endpoint === system.Endpoint  && user._systems_Type === system.Type){ 
			        	cnt++;
			        	if(!system._users){
			        		system._users = [];
			        		delete user._systems_Endpoint
			        		delete user._systems_Type
							system._users.push(user);
			        	}else{
			        		delete user._systems_Endpoint
			        		delete user._systems_Type
							system._users.push(user);
			        	}
			        }
				});
		    });
			customer.RegistrationNumber = customer.RegistrationNumber.replace(/[^0-9]/g, "");
		    if(cnt === customer._users.length){
				delete customer._users
				this._createCustomer(customer);
		    }else{
		    	MessageBox.warning(this.i18n.getText("create.notExistSystem"));
				this.getView().byId("createPage").setBusy(false);
		    }
		},
		
		_createCustomer: function(customer){
			var that = this;
			$.ajax({
				url: "/CI_IVISION_ODATA_V4/Admin/Customers",
				headers: {
					"Content-Type": "application/json;charset=utf-8"
				},
				data: JSON.stringify(customer),
				method: "POST",
				success: function(data, headers){
					MessageBox.success(that.i18n.getText("create.successSave"), {
						actions: [MessageBox.Action.OK],
						emphasizedAction: MessageBox.Action.OK,
						onClose: function (oAction) { 
							if(oAction === "OK"){
								that.getOwnerComponent().reload = true;
								that.Router.navTo("Main");
							}
						}
					});
					that.getView().byId("createPage").setBusy(false);
				},
				error: function(error){
					MessageBox.error(that.i18n.getText("create.error"));
					that.getView().byId("createPage").setBusy(false);
				}
			});			
		},
	});
});